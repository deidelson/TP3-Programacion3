package presentacion;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.Calendar;


import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.border.MatteBorder;

import com.toedter.calendar.JCalendar;

import datos.Conexion;
import negocio.ConjuntoOfertas;
import negocio.Oferta;
import java.awt.Font;


public class MainForm
{

	private JFrame frame;
	private JCalendar calendario;
	private Conexion conexion;
	private ConjuntoOfertas ofertas;
	private JTextField tbNombre;
	private JTextField tbIncio;
	private JTextField tbFin;
	private JTextField tbMonto;
	private JTextField tbEquipamiento;
	private JLabel lbllosCamposCon;
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args)
	{
		EventQueue.invokeLater(new Runnable()
		{
			public void run()
			{
				try
				{
					MainForm window = new MainForm();
					window.frame.setVisible(true);
				} 
				catch (Exception e)
				{
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public MainForm() 
	{
		initialize();
		conexion=new Conexion();
		ofertas=new ConjuntoOfertas();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() 
	{
		frame = new JFrame();
		frame.setForeground(Color.WHITE);
		frame.getContentPane().setForeground(Color.WHITE);
		frame.getContentPane().setBackground(new Color(0, 153, 204));
		frame.setBounds(100, 100, 692, 570);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		frame.setTitle("Reservas Sala de Ensayo");
		
		calendario = new JCalendar();
		calendario.setBackground(Color.WHITE);
		calendario.getDayChooser().setBackground(new Color(0, 0, 0));
		calendario.setBorder(new MatteBorder(2, 2, 2, 2, (Color) new Color(255, 0, 0)));
		calendario.setBounds(30, 55, 326, 303);
		frame.getContentPane().add(calendario);
	
		JButton btnVerOfertasDel = new JButton("Ver ofertas del dia");
		btnVerOfertasDel.setForeground(new Color(255, 255, 255));
		btnVerOfertasDel.setBackground(new Color(0, 139, 139));
		btnVerOfertasDel.setBounds(111, 385, 152, 23);
		btnVerOfertasDel.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e) 
			{
				if(verificarExistenciaArchivo()==true)
				{
					String nombre=obtenerFecha();
					ofertas=conexion.leerConjuntoOfertas(nombre);
					TablaOfertas tb=new TablaOfertas(ofertas);
					tb.verOfertas(ofertas);
				}
				else
				{
					JOptionPane.showMessageDialog(null, "No hay ofertas en este dia");
				}
				
			}
		});
		frame.getContentPane().add(btnVerOfertasDel);
		
		JButton btnNewButton = new JButton("Agregar oferta ");
		btnNewButton.setForeground(new Color(255, 255, 255));
		btnNewButton.setBackground(new Color(0, 139, 139));
		btnNewButton.setBounds(401, 385, 152, 23);
		btnNewButton.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				
				try
				{
					Calendar fechaActual= Calendar.getInstance();
					Calendar fechaCalendario=calendario.getCalendar();
					if(fechaActual.compareTo(fechaCalendario)>=0)
					{
							JOptionPane.showMessageDialog(null, "Las ofertas deben tener fecha posterior a la actual");
							return;
					}
						
					
					String horaInicio=tbIncio.getText();
					int hi=Integer.parseInt(horaInicio);
					String horaFin=tbFin.getText();
					int hf=Integer.parseInt(horaFin);
					String montoOfrecido=tbMonto.getText();
					double mo=Double.parseDouble(montoOfrecido);
				
					
					Oferta oferta = new Oferta(hi, hf, mo);
					String nombre=tbNombre.getText();
					oferta.setNombre(nombre);
					String equipamiento=tbEquipamiento.getText();
					oferta.setEquipamiento(equipamiento);
					
					
					if (verificarExistenciaArchivo()==true)
					{
						ofertas = conexion.leerConjuntoOfertas(obtenerFecha());
						ofertas.agregarOferta(oferta);
						conexion.generarJsonConjuntoOfertas(ofertas, obtenerFecha());
					}
					else
					{
						ofertas=new ConjuntoOfertas();
						ofertas.agregarOferta(oferta);
						conexion.generarJsonConjuntoOfertas(ofertas, obtenerFecha());
					}
					
					JOptionPane.showMessageDialog(null, "La oferta se ha guardado correctamente");
					tbNombre.setText("");
					tbIncio.setText("");
					tbFin.setText("");
					tbMonto.setText("");
				}
				catch(Exception s)
				{
					JOptionPane.showMessageDialog(null, "Ha ocurrido algun error, intente nuevamente");
				}
			}
		});
		frame.getContentPane().add(btnNewButton);
		
		tbNombre = new JTextField();
		tbNombre.setBounds(401, 68, 152, 20);
		frame.getContentPane().add(tbNombre);
		tbNombre.setColumns(10);
		
		tbIncio = new JTextField();
		tbIncio.setBounds(401, 124, 45, 20);
		frame.getContentPane().add(tbIncio);
		tbIncio.setColumns(10);
		
		tbFin = new JTextField();
		tbFin.setBounds(401, 185, 45, 20);
		frame.getContentPane().add(tbFin);
		tbFin.setColumns(10);
		
		tbMonto = new JTextField();
		tbMonto.setBounds(401, 231, 86, 20);
		frame.getContentPane().add(tbMonto);
		tbMonto.setColumns(10);
		
		JLabel lblNombre = new JLabel("Nombre");
		lblNombre.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblNombre.setBackground(Color.BLACK);
		lblNombre.setForeground(Color.WHITE);
		lblNombre.setBounds(401, 54, 46, 14);
		frame.getContentPane().add(lblNombre);
		
		JLabel lblHoraDeInicio = new JLabel("*Hora de inicio");
		lblHoraDeInicio.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblHoraDeInicio.setForeground(Color.WHITE);
		lblHoraDeInicio.setBounds(401, 110, 98, 14);
		frame.getContentPane().add(lblHoraDeInicio);
		
		JLabel lblHoraDeFin = new JLabel("*Hora de fin");
		lblHoraDeFin.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblHoraDeFin.setForeground(Color.WHITE);
		lblHoraDeFin.setBounds(401, 170, 86, 14);
		frame.getContentPane().add(lblHoraDeFin);
		
		JLabel lblMonto = new JLabel("*Monto");
		lblMonto.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblMonto.setForeground(Color.WHITE);
		lblMonto.setBounds(400, 216, 46, 14);
		frame.getContentPane().add(lblMonto);
		
		JLabel lblEquipamientoNecesario = new JLabel("Equipamiento necesario");
		lblEquipamientoNecesario.setForeground(Color.WHITE);
		lblEquipamientoNecesario.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblEquipamientoNecesario.setBounds(401, 273, 152, 14);
		frame.getContentPane().add(lblEquipamientoNecesario);
		
		tbEquipamiento = new JTextField();
		tbEquipamiento.setBounds(401, 287, 229, 20);
		frame.getContentPane().add(tbEquipamiento);
		tbEquipamiento.setColumns(10);
		
		lbllosCamposCon = new JLabel("(Los campos con * son obligatorios)");
		lbllosCamposCon.setForeground(Color.WHITE);
		lbllosCamposCon.setFont(new Font("Tahoma", Font.BOLD, 11));
		lbllosCamposCon.setBounds(401, 342, 229, 14);
		frame.getContentPane().add(lbllosCamposCon);
	}
	
	public String obtenerFecha()
	{
		int dia= calendario.getCalendar().get(Calendar.DAY_OF_MONTH);
		int mes= calendario.getCalendar().get(Calendar.MONTH)+1;
		int anio= calendario.getCalendar().get(Calendar.YEAR);
		
		String fecha=Integer.toString(anio)+Integer.toString(mes)+Integer.toString(dia);
		
		return fecha;
	}
	
	public boolean verificarExistenciaArchivo()
	{
		String info = obtenerFecha();
		String archivo="archivosOfertas\\"+info+".json";
		File f = new File(archivo);
		if(f.exists())
			return true;
		else
			return false;
	}
}
