package presentacion;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.FileDialog;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collections;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.MatteBorder;
import javax.swing.table.DefaultTableModel;

import datos.Conexion;
import negocio.ConjuntoOfertas;
import negocio.Oferta;
import negocio.Solver;
import negocio.Subconjunto;
import javax.swing.JButton;
import java.awt.SystemColor;


public class TablaOfertas {

	private JFrame frmOfertas;
	private JTable table;
	private ConjuntoOfertas ofertas;
	private Subconjunto solucion;
	private Conexion conexion;
	private boolean viendoOfertas;
	private boolean viendoSolucion;
	private JMenu mnResolver;
	private JMenuItem todasLasOfertas;

	/**
	 * Launch the application.
	 */
	public void verOfertas(ConjuntoOfertas o)
	{
		EventQueue.invokeLater(new Runnable() 
		{
			public void run() 
			{
				try
				{
					TablaOfertas window = new TablaOfertas(o);
					window.frmOfertas.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public TablaOfertas(ConjuntoOfertas o)
	{
		ofertas=o;
		solucion=new Subconjunto();
		conexion=new Conexion();
		viendoOfertas=true;
		viendoSolucion=false;
		initialize();
		
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() 
	{
		frmOfertas = new JFrame();
		frmOfertas.getContentPane().setBackground(new Color(51, 153, 204));
		frmOfertas.setBounds(100, 100, 696, 482);
		frmOfertas.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frmOfertas.getContentPane().setLayout(null);
		frmOfertas.setTitle("Ofertas");
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBackground(SystemColor.desktop);
		scrollPane.setBounds(10, 32, 668, 357);
		frmOfertas.getContentPane().add(scrollPane);
		
		table = new JTable();
		int cantidad=24;
		if(ofertas.cantidadDeOfertas()>cantidad)
			cantidad=ofertas.cantidadDeOfertas();
		table.setModel(new DefaultTableModel(
			new Object[cantidad][5],
			new String[] {
				"Nombre","Hora Inicio", "Hora Fin", "Monto Ofrecido", "Equipamiento"
			}
		));
		
		table.setRowHeight(26);
		table.setFont(new Font("Tahoma", Font.BOLD, 14));
		table.setBackground(new Color(0, 0, 51));
		table.setBorder(new MatteBorder(1, 2, 2, 2, (Color) new Color(169, 169, 169)));
		table.setEnabled(false);
		table.setForeground(new Color(255, 255, 255));
		table.setRowSelectionAllowed(false);
		scrollPane.setViewportView(table);
		
		mostrarOfertasDelDia();
		
		JMenuBar menuBar = new JMenuBar();
		menuBar.setBounds(0, 0, 153, 21);
		frmOfertas.getContentPane().add(menuBar);
		
		JMenu mnArchivo = new JMenu("Archivo");
		menuBar.add(mnArchivo);
		
		JMenuItem mntmCargar = new JMenuItem("Cargar soluci\u00F3n");
		mntmCargar.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0) 
			{
				//TODO agregar bloqueo de botones
				try
				{
					FileDialog explorador=new FileDialog(frmOfertas, "Eleg� el archivo que deseas cargar", FileDialog.LOAD);
					explorador.setDirectory("archivosSoluciones\\");
					explorador.setVisible(true);
					
					if(explorador.getFile()!= null)
					{
						String nombreDelArchivo=explorador.getFile();
						solucion=conexion.leerSolucion(nombreDelArchivo);
						borrarTabla();
						mostrarSolucion();
					}
					viendoOfertas=false;
					viendoSolucion=true;
					mnResolver.setEnabled(false);
					todasLasOfertas.setEnabled(false);
				}
				catch(Exception e)
				{
					borrarTabla();
					JOptionPane.showMessageDialog(null, "Algo sal�o mal");
				}
			}
		});
		mnArchivo.add(mntmCargar);
		
		JMenuItem mntmGuardar = new JMenuItem("Guardar soluci\u00F3n");
		mntmGuardar.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0) 
			{
				try
				{
					if(solucion.cantidadDeOfertas()>0)
					{
						String nombreArchivo=JOptionPane.showInputDialog("Ingrese el nombre que desea darle al archivo");
						conexion.guardarSolucion(solucion, nombreArchivo);
						JOptionPane.showMessageDialog(null, "Se guard� correctamente");
					}
					else
						JOptionPane.showMessageDialog(null, "Primero debe generar una soluci�n");
				
				}
				catch(Exception s)
				{
					JOptionPane.showMessageDialog(null, "Ha ocurrido alg�n error");
				}
			}
		});
		mnArchivo.add(mntmGuardar);
		
		JMenu mnNewMenu = new JMenu("Ver");
		menuBar.add(mnNewMenu);
		
		todasLasOfertas = new JMenuItem("Todas las ofertas");
		todasLasOfertas.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0) 
			{
				borrarTabla();
				mostrarOfertasDelDia();
			}
		});
		mnNewMenu.add(todasLasOfertas);
		
		JMenu mnOrdenad = new JMenu("Ordenar");
		mnNewMenu.add(mnOrdenad);
		
		JMenuItem mntmPorHoraDe = new JMenuItem("Por hora de inicio");
		mntmPorHoraDe.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				ArrayList<Oferta>ofer=obtenerOfertas();
				Collections.sort(ofer,(o1, o2) -> {return comparadorPorHoraInicio(o1, o2);});
				borrarTabla();
				mostrarArrayList(ofer);
			}

			
		});
		mnOrdenad.add(mntmPorHoraDe);
		
		JMenuItem mntmPorHoraDe_1 = new JMenuItem("Por hora de fin");
		mntmPorHoraDe_1.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e)
			{
				ArrayList<Oferta>ofer=obtenerOfertas();
				Collections.sort(ofer, (o1, o2) -> {return comparadorPorHoraFin(o1, o2);});
				borrarTabla();
				mostrarArrayList(ofer);
			}

		
		});
		mnOrdenad.add(mntmPorHoraDe_1);
		
		JMenuItem mntmPorMonto_1 = new JMenuItem("Por monto");
		mntmPorMonto_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) 
			{
				ArrayList<Oferta>ofer=obtenerOfertas();
				Collections.sort(ofer, (o1, o2) -> {return comparadorPorMonto(o1, o2);});
				borrarTabla();
				mostrarArrayList(ofer);
			}
		});
		
		JMenuItem mntmPorCantidadDe = new JMenuItem("Por cantidad de horas");
		mntmPorCantidadDe.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e)
			{
				ArrayList<Oferta>ofer=obtenerOfertas();
				Collections.sort(ofer,(o1, o2) -> {return comparadorPorCantidadHoras(o1, o2);});
				borrarTabla();
				mostrarArrayList(ofer);
			}
		});
		mnOrdenad.add(mntmPorCantidadDe);
		mnOrdenad.add(mntmPorMonto_1);
		
		mnResolver = new JMenu("Resolver");
		menuBar.add(mnResolver);
		
		JMenuItem mntmPorHoras = new JMenuItem("Por horas");
		mntmPorHoras.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e)
			{
				try
				{
					Solver solver= new Solver((o1, o2) ->{return comparadorPorCantidadHoras(o1, o2);});
					solucion=solver.resolver(ofertas);
					borrarTabla();
					mostrarSolucion();
				}
				catch(Exception s)
				{
					JOptionPane.showConfirmDialog(null, "Ocurrio alg�n error");
				}
			}

			
		});
		mnResolver.add(mntmPorHoras);
		
		JMenuItem mntmPorMonto = new JMenuItem("Por monto");
		mntmPorMonto.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				try
				{
					Solver solver= new Solver ((o1, o2) -> {return comparadorPorMonto(o1, o2);});
					solucion=solver.resolver(ofertas);
					borrarTabla();
					mostrarSolucion();
				}
				catch(Exception s)
				{
					JOptionPane.showConfirmDialog(null, "Ocurrio alg�n error");
				}
			}
		});
		mnResolver.add(mntmPorMonto);
		
		JMenuItem mntmPorCociente = new JMenuItem("Por cociente");
		mntmPorCociente.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				try
				{
					Solver solver= new Solver ((o1, o2) -> { return comparadorPorCociente(o1, o2);});
					solucion=solver.resolver(ofertas);
					borrarTabla();
					mostrarSolucion();
				}
				catch(Exception s)
				{
					JOptionPane.showConfirmDialog(null, "Ocurrio alg�n error");
				}
			}

			
		});
		mnResolver.add(mntmPorCociente);
		
		JButton btnNewButton = new JButton("Regresar");
		btnNewButton.setForeground(new Color(255, 255, 255));
		btnNewButton.setBackground(new Color(0, 139, 139));
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				frmOfertas.dispose();
			}
		});
		btnNewButton.setBounds(262, 400, 146, 23);
		frmOfertas.getContentPane().add(btnNewButton);

		
	}
	
	
	//comparadores
	private int comparadorPorCantidadHoras(Oferta o1, Oferta o2)
	{
		return (o1.getHoraFin()-o1.getHoraInicio())-(o2.getHoraFin()-o2.getHoraInicio());
	}
	
	private int comparadorPorMonto(Oferta o1, Oferta o2)
	{
		return(int) (o2.getMontoDispuesto()-o1.getMontoDispuesto());
	}
	
	private int comparadorPorCociente(Oferta o1, Oferta o2) 
	{
		int t1=o1.getHoraFin()-o1.getHoraInicio();
		int t2=o2.getHoraFin()-o2.getHoraInicio();
		if(t1 < t2)
			return -1;
		if(t1 > t2)
			return 1;
		else		
			return 0;
	}
	
	private int comparadorPorHoraInicio(Oferta o1, Oferta o2)
	{
		return o1.getHoraInicio()-o2.getHoraInicio();
	}
	
	private int comparadorPorHoraFin(Oferta o1, Oferta o2) 
	{
		return o1.getHoraFin()-o2.getHoraFin();
	}
	
	//metodos para mostrar las ofertas
	public void mostrarOfertasDelDia()
	{
		for (int i = 0; i<5;i++)
			for (int j = 0; j<ofertas.cantidadDeOfertas();j++)
				if (i==0)
					table.setValueAt(ofertas.getOferta(j).getNombre(), j, 0);
				else if (i==1)
					table.setValueAt(ofertas.getOferta(j).getHoraInicio(), j, 1);
				else if (i==2)
					table.setValueAt(ofertas.getOferta(j).getHoraFin(), j, 2);
				else if(i==3)
					table.setValueAt("$ "+ofertas.getOferta(j).getMontoDispuesto(), j, 3);
				else 
					table.setValueAt(ofertas.getOferta(j).getEquipamiento(), j, 4);
		this.viendoOfertas=true;
		this.viendoSolucion=false;
	}	
	
	public void mostrarSolucion()
	{
		for (int i = 0; i<5;i++)
			for (int j = 0; j<solucion.cantidadDeOfertas();j++)
				if (i==0)
					table.setValueAt(solucion.getOferta(j).getNombre(), j, 0);
				else if (i==1)
					table.setValueAt(solucion.getOferta(j).getHoraInicio(), j, 1);
				else if (i==2)
					table.setValueAt(solucion.getOferta(j).getHoraFin(), j, 2);
				else if (i==3)
					table.setValueAt("$ "+solucion.getOferta(j).getMontoDispuesto(), j, 3);
				else 
					table.setValueAt(solucion.getOferta(j).getEquipamiento(), j, 4);
		this.viendoOfertas=false;
		this.viendoSolucion=true;
	}
	
	public void mostrarArrayList(ArrayList<Oferta>ofer)
	{
		for (int i = 0; i<5;i++)
			for (int j = 0; j<ofer.size();j++)
				if (i==0)
					table.setValueAt(ofer.get(j).getNombre(), j, 0);
				else if (i==1)
					table.setValueAt(ofer.get(j).getHoraInicio(), j, 1);
				else if (i==2)
					table.setValueAt(ofer.get(j).getHoraFin(), j, 2);
				else if(i==3)
					table.setValueAt("$ "+ofer.get(j).getMontoDispuesto(), j, 3);
				else 
					table.setValueAt(ofer.get(j).getEquipamiento(), j, 4);
	}
	
	public void borrarTabla()
	{
		int cantidad=24;
		
		if(ofertas.cantidadDeOfertas()>24)
			cantidad=ofertas.cantidadDeOfertas();
		
		for (int i = 0; i<5;i++)
			for (int j = 0; j<cantidad;j++)
				if (i==0)
					table.setValueAt("", j, 0);
				else if (i==1)
					table.setValueAt("", j, 1);
				else if (i==2)
					table.setValueAt("", j, 2);
				else if(i==3)
					table.setValueAt("", j, 3);
				else
					table.setValueAt("", j, 4);
	}
	
	
	public ArrayList<Oferta> obtenerOfertas()
	{
		ArrayList<Oferta>ofer;
		if(viendoOfertas==true && viendoSolucion==false)
			ofer=ofertas.getOfertas();
		else
			ofer=solucion.getOfertas();
		return ofer;
	}

	
}
