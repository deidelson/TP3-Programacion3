package datos;

import static org.junit.Assert.*;
import negocio.ConjuntoOfertas;
import negocio.Oferta;
import negocio.Subconjunto;

import org.junit.Test;

public class TestConexion {

	@Test
	public void pruebaJsonConjuntoOfertas() 
	{
		ConjuntoOfertas ofertas=new ConjuntoOfertas();
		
		Conexion conexion=new Conexion();
		
		ofertas.agregarOferta(new Oferta(0, 6, 350));
		ofertas.agregarOferta(new Oferta(6, 12, 450));
		ofertas.agregarOferta(new Oferta(12, 18, 650));
		ofertas.agregarOferta(new Oferta(18, 24, 900));
		
		conexion.generarJsonConjuntoOfertas(ofertas, "prueba");
		
		ConjuntoOfertas co=conexion.leerConjuntoOfertas("prueba");
		
		for(int i=0; i<3; i++)
		{
			assertEquals(ofertas.getOferta(i), co.getOferta(i));
		}
	}
	
	@Test 
	public void pruebaJsonSubconjunto()
	{
		//Cuando se arregle subconjunto va a funcionar
		Subconjunto ofertas=new Subconjunto();
		
		Conexion conexion=new Conexion();
		
		ofertas.agregarOferta(new Oferta(0, 6, 350));
		ofertas.agregarOferta(new Oferta(6, 12, 450));
		ofertas.agregarOferta(new Oferta(12, 18, 650));
		ofertas.agregarOferta(new Oferta(18, 24, 900));
		
		conexion.guardarSolucion(ofertas, "pruebaSub");
		
		
	}

}
