package datos;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.lang.reflect.Type;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import negocio.ConjuntoOfertas;
import negocio.Subconjunto;



public class Conexion 
{
	public Conexion()
	{
		
	}
	
	public void guardarSolucion(Subconjunto oferta, String nombreDelArchivo)
	{
		Gson gson= new GsonBuilder().setPrettyPrinting().create();
		String json=gson.toJson(oferta);
		
		try
		{
			FileWriter writer=new FileWriter("archivosSoluciones\\"+nombreDelArchivo+".json");
			writer.write(json);
			writer.close();
		}
		catch(Exception e){}
	}
	
	public Subconjunto leerSolucion(String nombreDelArchivo)
	{
		Subconjunto ret=null;
		Gson gson=new Gson();
		//nombreDelArchivo+=".json";
		 try
		 {
			 Type tipo = new TypeToken<Subconjunto>(){}.getType();
			 BufferedReader br = new BufferedReader(new FileReader("archivosSoluciones\\"+nombreDelArchivo));
			 ret=gson.fromJson(br, tipo);
		 }
		 catch (Exception e) {}
		 return ret;
	}
	
	public void generarJsonConjuntoOfertas(ConjuntoOfertas oferta, String nombreDelArchivo)
	{
		Gson gson= new GsonBuilder().setPrettyPrinting().create();
		String json=gson.toJson(oferta);
		
		try
		{
			FileWriter writer=new FileWriter("archivosOfertas\\"+nombreDelArchivo+".json");
			writer.write(json);
			writer.close();
		}
		catch(Exception e){}
	}
	
	public ConjuntoOfertas leerConjuntoOfertas(String nombreDelArchivo)
	{
		ConjuntoOfertas ret=null;
		Gson gson=new Gson();
		nombreDelArchivo+=".json";
		 try
		 {
			 Type tipo = new TypeToken<ConjuntoOfertas>(){}.getType();
			 BufferedReader br = new BufferedReader(new FileReader("archivosOfertas\\"+nombreDelArchivo));
			 ret=gson.fromJson(br, tipo);
		 }
		 catch (Exception e) {}
		 return ret;
	}
	


}
