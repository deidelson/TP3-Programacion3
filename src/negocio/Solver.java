package negocio;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;


public class Solver
{
	private Comparator<Oferta> comparator;
	
	public Solver(Comparator<Oferta> comp)
	{
		comparator=comp;
	}
	
	public Subconjunto resolver(ConjuntoOfertas ofertas)
	{
		Subconjunto ret = new Subconjunto();
		
		for(Oferta oferta:ofertasOrdenadas(ofertas))
		{
			int horas=oferta.getHoraFin()-oferta.getHoraInicio();
			if(ret.getCantidadDeHoras()+horas <= 24)
				if(ret.intersectaConAlguna(oferta)==false)
					ret.agregarOferta(oferta);
		}
		return ret;
	}
	
	ArrayList<Oferta> ofertasOrdenadas(ConjuntoOfertas ofertas)
	{
		ArrayList<Oferta> ret = ofertas.getOfertas();
		Collections.sort(ret, comparator);
		return ret;
	}
	

}
