package negocio;

import static org.junit.Assert.*;

import org.junit.Test;

public class TestSolver
{
	@Test
	public void solucionPorDinero() 
	{
		ConjuntoOfertas co=crearConjuntoOfertas();
		Solver solver= new Solver
				((o1, o2) -> 
					{
						return(int) (o2.getMontoDispuesto()-o1.getMontoDispuesto());
					}
				);
		Subconjunto sol= solver.resolver(co);
		
		assertTrue(sol.getMontoDispuesto()==1550);
		assertTrue(sol.getCantidadDeHoras()==24);
		assertTrue(sol.cantidadDeOfertas()==2);
	}
	
	@Test
	public void solucionPorHoras() 
	{
		ConjuntoOfertas co=crearConjuntoOfertas();
		Solver solver= new Solver
				((o1, o2) -> 
				{
					return (o1.getHoraFin()-o1.getHoraInicio())-(o2.getHoraFin()-o2.getHoraInicio());
				}
				);
		Subconjunto sol= solver.resolver(co);
		
		assertTrue(sol.getMontoDispuesto()==1500);
		assertTrue(sol.getCantidadDeHoras()==24);
		assertTrue(sol.cantidadDeOfertas()==4);
	}
	

	@Test
	public void solucionPorCociente() 
	{
		ConjuntoOfertas co=crearConjuntoOfertas();
		Solver solver= new Solver
				((o1, o2) ->
					{ 
						int t1=o1.getHoraFin()-o1.getHoraInicio();
						int t2=o2.getHoraFin()-o2.getHoraInicio();
						if(t1 < t2)
							return -1;
						if(t1 > t2)
							return 1;
						else		
							return 0;
					}
				);
		Subconjunto sol= solver.resolver(co);
		
		assertTrue(sol.getMontoDispuesto()==1500);
		assertTrue(sol.getCantidadDeHoras()==24);
		assertTrue(sol.cantidadDeOfertas()==4);
	}
	

	public ConjuntoOfertas crearConjuntoOfertas()
	{
		ConjuntoOfertas c=new ConjuntoOfertas();
		
		c.agregarOferta(new Oferta(13, 24, 150));
		c.agregarOferta(new Oferta(0, 6, 200));
		c.agregarOferta(new Oferta(6, 12, 300));
		c.agregarOferta(new Oferta(1, 8, 50));
		c.agregarOferta(new Oferta(12, 18, 400));
		c.agregarOferta(new Oferta(18, 24, 600));
		c.agregarOferta(new Oferta(0, 12, 700));
		c.agregarOferta(new Oferta(12, 24, 850));
		
		
		
		
		return c;
	}

}
