package negocio;

import static org.junit.Assert.*;

import org.junit.Test;

public class TestOferta {

	@Test
	public void constructor()//getters tambien
	{
		Oferta o=crearInstancia();
		
		assertEquals(o.getHoraInicio(), 0);
		assertEquals(o.getHoraFin(), 6);
		assertTrue(o.getMontoDispuesto()==250);
	}
	
	
	//OK
	@SuppressWarnings("unused")
	@Test (expected = IllegalArgumentException.class)
	public void constructorExtremos()
	{
		Oferta o=new Oferta(-1, 12, 350);
		Oferta o1=new Oferta(12, 11, 350);
		Oferta o2=new Oferta(1, -5, 350);
		Oferta o3=new Oferta(1, 2, -350);
		Oferta o4=new Oferta(-1, -5, -350);
	}
	
	@Test 
	public void settersYgetters()
	{
		Oferta o=crearInstancia();
		
		o.setHoraFin(12);
		o.setHoraInicio(6);
		o.setMontoDispuesto(350);
		
		assertEquals(o.getHoraInicio(), 6);
		assertEquals(o.getHoraFin(), 12);
		assertTrue(o.getMontoDispuesto()==350);
	}
	
	//OK
	@Test (expected = IllegalArgumentException.class)
	public void rompiendoSettersyGetters()
	{
		Oferta o=new Oferta(12, 16, 250);
		
		o.setHoraFin(11);
		o.setHoraInicio(36);
		o.setHoraFin(54);
		o.setHoraInicio(-9);
		o.setHoraFin(-7);
		o.setMontoDispuesto(-350);
		
	}
	
	@Test
	public void equals()
	{
		Oferta of1=new Oferta(12, 16, 250);
		Oferta of2=new Oferta(12, 16, 250);
		
		
		assertEquals(of1, of2);
		of2.setHoraFin(15);
		assertNotEquals(of1, of2);
		of2.setHoraInicio(13);
		of2.setHoraFin(16);
		assertNotEquals(of1, of2);
		of2.setHoraInicio(12);
		of2.setMontoDispuesto(246);
		assertNotEquals(of1, of2);
		of2.setMontoDispuesto(250);
		assertEquals(of1, of2);
		of2.setNombre("a");
		assertNotEquals(of1, of2);
	}
	
	
	
	//instancias
	
	public Oferta crearInstancia()
	{
		Oferta o = new Oferta(0,6,250);
		
		return o;
	}

}
