package negocio;



public class Oferta {
	private String nombre;
	private int horaInicio;
	private int horaFin;
	private double montoDispuesto;
	private String equipamiento;
	
	
	public Oferta(int inicio, int fin, double monto)
	{
		
		if (inicio<0 || inicio>=24 || inicio>fin)
			throw new IllegalArgumentException();
		
		if (fin<=0 || fin>24 ||fin<inicio)
			throw new IllegalArgumentException();
		
		if (monto<=0)
			throw new IllegalArgumentException();
		if(inicio==fin)
			throw new IllegalArgumentException();
		this.nombre="";
		this.horaInicio=inicio;
		this.horaFin=fin;
		this.montoDispuesto=monto;
		this.equipamiento="";
	}
	
	public Oferta()
	{
		this.nombre="";
		this.horaInicio=0;
		this.horaFin=0;
		this.montoDispuesto=0;
	}
	
	public void setNombre(String nom)
	{
		this.nombre=nom;
	}
	
	public String getNombre()
	{
		return this.nombre;
	}
	
	public void setHoraInicio(int hora)
	{
		if (hora<0 || hora>=24)
			throw new IllegalArgumentException();
		
		this.horaInicio=hora;
	}
	
	public void setHoraFin(int hora)
	{
		if (hora<=this.horaInicio || hora<=0 || hora>24)
			throw new IllegalArgumentException();
		
		this.horaFin=hora;
	}
	
	public void setMontoDispuesto(double monto)
	{
		if (monto<=0)
			throw new IllegalArgumentException();
		
		this.montoDispuesto=monto;
	}
	
	public String getEquipamiento() 
	{
		return equipamiento;
	}

	public void setEquipamiento(String equipamiento)
	{
		this.equipamiento = equipamiento;
	}
	
	public int getHoraInicio()
	{
		return this.horaInicio;
	}
	
	public int getHoraFin()
	{
		return this.horaFin;
	}
	
	public double getMontoDispuesto()
	{
		return this.montoDispuesto;
	}
	
	public int getCantidadDeHoras()
	{
		return this.horaFin-horaInicio;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Oferta other = (Oferta) obj;
		if(nombre.equals( other.nombre)==false)
			return false;
		if (horaFin != other.horaFin)
			return false;
		if (horaInicio != other.horaInicio)
			return false;
		if (montoDispuesto != other.montoDispuesto)
			return false;
		return true;
	}
	
}
