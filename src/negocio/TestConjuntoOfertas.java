package negocio;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;

import org.junit.Test;

public class TestConjuntoOfertas
{

	@Test//no lo trato de romper hasta que no este corregido el contructor de Oferta
	public void agregarOfertas()
	{
		ConjuntoOfertas co = crearInstancia();
		
		assertEquals(new Oferta(12, 16, 250), co.getOferta(0));
	}

	@Test(expected = Exception.class)
	public void rompiendoGetOferta()
	{
		ConjuntoOfertas co=crearInstancia();
		
		co.getOferta(-1);
		co.getOferta(3);
	}
	
	@Test
	public void cantidadDeOfertas()
	{
		ConjuntoOfertas o= crearInstancia();
		
		assertTrue(o.cantidadDeOfertas()==3);
	}
	
	@Test
	public void getOfertas()
	{
		ConjuntoOfertas o= crearInstancia();
		ArrayList<Oferta> a=o.getOfertas();
		a.size();
		ArrayList<Oferta> of=o.getOfertas();
		
		of.remove(0);
		
		assertTrue(o.cantidadDeOfertas()==3);
		
	}
	
	
	//intancias
	private ConjuntoOfertas crearInstancia()
	{
		ConjuntoOfertas co=new ConjuntoOfertas();
		co.agregarOferta(new Oferta(12, 16, 250));
		co.agregarOferta(new Oferta(16, 18, 600));
		co.agregarOferta(new Oferta(10, 12, 680));
		return co;
	}
	
	

}
