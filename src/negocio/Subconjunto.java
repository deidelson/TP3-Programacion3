package negocio;

import java.util.ArrayList;


public class Subconjunto {

	ArrayList <Oferta> ofertas;
	int cantidadDeHoras;
	double montoDispuesto;
	
	public Subconjunto()
	{
		this.ofertas= new ArrayList <Oferta>();
		this.cantidadDeHoras=0;
		this.montoDispuesto=0;
	}
	
	public Oferta getOferta(int j) 
	{
		return ofertas.get(j);
	}
	
	public int getCantidadDeHoras()
	{
		return this.cantidadDeHoras;
	}
	
	public double getMontoDispuesto()
	{
		return this.montoDispuesto;
	}
	
	public void agregarOferta(Oferta oferta)
	{
		this.ofertas.add(oferta);
		this.cantidadDeHoras+=oferta.getCantidadDeHoras();
		this.montoDispuesto+=oferta.getMontoDispuesto();
		
	}
	
	public void quitarOferta(Oferta oferta)
	{
		if(this.contiene(oferta))
		{
			this.cantidadDeHoras-=(oferta.getCantidadDeHoras());
			this.montoDispuesto-=(oferta.getMontoDispuesto());
		}
		ofertas.remove(oferta);
	}
	
	public boolean intersectaConAlguna(Oferta oferta)
	{
		boolean ac=false;
		for (Oferta ofer : ofertas)
			
		{
			ac=ac || (oferta.getHoraInicio() > ofer.getHoraInicio() && oferta.getHoraInicio() <ofer.getHoraFin());
				
			ac=ac || (oferta.getHoraFin() <=ofer.getHoraFin() && oferta.getHoraFin() > ofer.getHoraInicio());
			
			ac=ac || (oferta.getHoraInicio() >= ofer.getHoraInicio() && oferta.getHoraInicio() <ofer.getHoraFin());
			
			ac=ac || (oferta.getHoraInicio()==ofer.getHoraInicio() && oferta.getHoraFin() == ofer.getHoraFin());
				
			ac=ac || (oferta.getHoraInicio()<ofer.getHoraInicio() && oferta.getHoraFin() > ofer.getHoraFin());
		}
		return ac;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Subconjunto other = (Subconjunto) obj;
		if (cantidadDeHoras != other.cantidadDeHoras)
			return false;
		if ((montoDispuesto) != (other.montoDispuesto))
			return false;
		if (ofertas == null) {
			if (other.ofertas != null)
				return false;
		} else if (!ofertas.equals(other.ofertas))
			return false;
		return true;
	}
	
	public boolean contiene (Oferta of)
	{
		for (Oferta i : ofertas)
			if (i.equals(of)) return true;
		return false;
	}
	
	public int cantidadDeOfertas()
	{
		return ofertas.size();
	}

	@SuppressWarnings("unchecked")
	public ArrayList<Oferta> getOfertas() 
	{
		return (ArrayList<Oferta>) ofertas.clone();
	}

}


