package negocio;

import java.util.ArrayList;


public class ConjuntoOfertas
{
	private ArrayList<Oferta> ofertas;
	
	public ConjuntoOfertas()
	{
		ofertas=new ArrayList<Oferta>();
	}
	
	public void agregarOferta(Oferta oferta)
	{
		this.ofertas.add(oferta);
	}
	
	public Oferta getOferta(int indice)
	{
		return ofertas.get(indice);
	}
	
	public int cantidadDeOfertas()
	{
		return this.ofertas.size();
	}
	
	@SuppressWarnings("unchecked")
	public ArrayList <Oferta> getOfertas()
	{
		return (ArrayList<Oferta>) this.ofertas.clone();
	}
}
