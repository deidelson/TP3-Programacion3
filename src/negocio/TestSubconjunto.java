package negocio;

import static org.junit.Assert.*;

import org.junit.Test;

public class TestSubconjunto {

	@Test
	public void pruebaConstructor()
	{
		Subconjunto ret=crearInstancia();
		assertTrue(ret.cantidadDeOfertas()==4);
		assertFalse(ret.cantidadDeOfertas()==0);
		
		assertFalse(ret.ofertas.get(0)==ret.ofertas.get(1));
		assertFalse(ret.ofertas.get(1)==ret.ofertas.get(3));
		assertFalse(ret.ofertas.get(2)==ret.ofertas.get(3));
		assertFalse(ret.ofertas.get(3)==ret.ofertas.get(2));
		
		assertTrue(ret.ofertas.get(0)==ret.ofertas.get(0));
		assertTrue(ret.ofertas.get(1)==ret.ofertas.get(1));
		assertTrue(ret.ofertas.get(2)==ret.ofertas.get(2));
		assertTrue(ret.ofertas.get(3)==ret.ofertas.get(3));
		
	}
	
	
	@Test (expected = IllegalArgumentException.class)
	public void pruebaConExtremos()
	{
		Subconjunto ret=crearInstancia();
		ret.agregarOferta(new Oferta(24, 25, 350));
		ret.agregarOferta(new Oferta(1, 0, 350));
		ret.agregarOferta(new Oferta(1, 2, 0));
		ret.agregarOferta(new Oferta (1, 4, -100));
		ret.agregarOferta(new Oferta (1, 25, 100));
	}
	
	@Test
	public void intersectaConAlguna()
	{
		Subconjunto s=crearInstancia();
		
		assertTrue(s.intersectaConAlguna(new Oferta(0, 6, 10)));
		assertTrue(s.intersectaConAlguna(new Oferta(0, 5, 10)));
		assertTrue(s.intersectaConAlguna(new Oferta(1, 6, 10)));
		assertTrue(s.intersectaConAlguna(new Oferta(1, 5, 10)));
		assertTrue(s.intersectaConAlguna(new Oferta(5, 7, 10)));
		assertTrue(s.intersectaConAlguna(new Oferta(12, 18, 10)));
		assertTrue(s.intersectaConAlguna(new Oferta(13, 18, 10)));
		assertTrue(s.intersectaConAlguna(new Oferta(12, 17, 10)));
		assertTrue(s.intersectaConAlguna(new Oferta(11, 17, 10)));
		
	}
	
	@Test 
	public void quitarOferta()
	{
		Subconjunto s = crearInstancia();
		s.quitarOferta(new Oferta(6, 12, 450));
		assertTrue(s.getCantidadDeHoras()==18);
		assertTrue(s.getMontoDispuesto()==2050);
		assertTrue(s.cantidadDeOfertas()==3);
	}
	
	//intancias
	
	public Subconjunto crearInstancia()
	{
		Subconjunto ret = new Subconjunto();
		ret.agregarOferta(new Oferta(0, 6, 350));
		ret.agregarOferta(new Oferta(6, 12, 450));
		ret.agregarOferta(new Oferta(12, 18, 650));
		ret.agregarOferta(new Oferta(18, 24, 1050));
		return ret;
	}

}
